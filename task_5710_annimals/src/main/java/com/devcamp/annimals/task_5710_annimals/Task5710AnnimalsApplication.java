package com.devcamp.annimals.task_5710_annimals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5710AnnimalsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5710AnnimalsApplication.class, args);
	}

}
