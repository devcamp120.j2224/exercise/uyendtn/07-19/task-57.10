package com.devcamp.annimals.task_5710_annimals.model;

public abstract class Annimals {
    private int age;
    private String gender;

    public abstract void isMammal();

    public void mate(){
        System.out.println("annimal mating ...");
    }

    public Annimals() {
    }

    public Annimals(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    
}
