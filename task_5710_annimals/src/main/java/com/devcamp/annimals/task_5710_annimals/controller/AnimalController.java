package com.devcamp.annimals.task_5710_annimals.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.annimals.task_5710_annimals.model.Annimals;
import com.devcamp.annimals.task_5710_annimals.model.Duck;
import com.devcamp.annimals.task_5710_annimals.model.Fish;
import com.devcamp.annimals.task_5710_annimals.model.Zebra;

@RestController
public class AnimalController {
    @CrossOrigin
    @GetMapping("listAnimal")
    public ArrayList<Annimals> listAnimal() {
        ArrayList<Annimals> lstAnimal = new ArrayList<Annimals>();

        Annimals duck = new Duck();
        duck.setAge(2);
        duck.setGender("male");
        ((Duck)duck).setBeakColor("yellow");
        duck.mate();
        duck.isMammal();

        Annimals fish = new Fish(3, "female", 3, true);
        fish.mate();
        fish.isMammal();

        Annimals zebra  = new Zebra(true);
        zebra.setAge(4);
        zebra.setGender("male");
        ((Zebra)zebra).run();

        lstAnimal.add(duck);
        lstAnimal.add(fish);
        lstAnimal.add(zebra);
        return lstAnimal;
    }
}
