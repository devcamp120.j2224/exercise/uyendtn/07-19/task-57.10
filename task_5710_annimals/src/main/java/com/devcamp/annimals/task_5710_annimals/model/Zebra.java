package com.devcamp.annimals.task_5710_annimals.model;

public class Zebra extends Annimals {
    private boolean is_wild;

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("zebra is mammal!");
    }
    
    public void run() {
        System.out.println("zebra is running...");
    }

    public Zebra(boolean is_wild) {
        super();
        this.is_wild = is_wild;
    }

    public Zebra(int age, String gender, boolean is_wild) {
        super(age, gender);
        this.is_wild = is_wild;
    }

    public boolean isIs_wild() {
        return is_wild;
    }

    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }
}
