package com.devcamp.annimals.task_5710_annimals.model;

public class Duck extends Annimals {
    private String beakColor;
    
    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Duck is mammal!");;
    }

    public void swim() {
        System.out.println("Duck is swimming...");
    }

    public void quack() {
        System.out.println("Duck is quacking...");
    }

    public Duck(String beakColor) {
        super();
        this.beakColor = beakColor;
    }


    public Duck(int age, String gender, String beakColor) {
        super(age, gender);
        this.beakColor = beakColor;
    }

    public String getBeakColor() {
        return beakColor;
    }

    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }

    public Duck() {
    }

    
    
}
